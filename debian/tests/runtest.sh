#!/bin/sh

set -e

trap "journalctl -u defectdojo | tail -n 100" EXIT

sudo defectdojo

if ! systemctl is-active -q defectdojo; then
    echo "The service defectdojo fails to start"
    exit 1
fi

if ! systemctl is-active -q defectdojo-uwsgi; then
    echo "The service defectdojo-uwsgi fails to start"
    exit 1
fi

if ! systemctl is-active -q defectdojo-celerybeat; then
    echo "The service defectdojo-celerybeat fails to start"
    exit 1
fi

if ! systemctl is-active -q defectdojo-celeryworker; then
    echo "The service defectdojo-celeryworker fails to start"
    exit 1
fi
